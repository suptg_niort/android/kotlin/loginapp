package com.enrickenet.loginapp

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity

class EcranDeDemarrage: AppCompatActivity() {

    val SPLASH_TIME_OUT = 3000 // = 3 secondes

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ecran_de_demarrage)

        /** Paramétrage de l'écran de chargement de l'application **/
        val ecranConnexion = Intent(this@EcranDeDemarrage, ConnexionUtilisateur::class.java)
        Handler().postDelayed({
            startActivity(ecranConnexion)
            finish()
        }, SPLASH_TIME_OUT.toLong())
    }
}