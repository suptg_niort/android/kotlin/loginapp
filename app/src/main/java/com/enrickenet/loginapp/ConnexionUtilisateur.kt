package com.enrickenet.loginapp

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty

class ConnexionUtilisateur : AppCompatActivity() {

    lateinit var edIdentifiant : EditText
    lateinit var edMotDePasse : EditText
    lateinit var valider : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_connexion_utilisateur)

        edIdentifiant = findViewById(R.id.ed_identifiant)
        edMotDePasse = findViewById(R.id.ed_motDePasse)
        valider = findViewById(R.id.btn_connexion)

        verificationConnexionInternet()

        /** Vérification des identifiants de connexion lors du clic sur le bouton "valider" **/
        valider.setOnClickListener {
            val identifiant = edIdentifiant.text.toString()
            val motDePasse = edMotDePasse.text.toString()

            if ((identifiant.equals("")) && (motDePasse.equals(""))) {
                val ecranAccueil = Intent(this, MainActivity::class.java)
                startActivity(ecranAccueil)
                val toastConnexionOK = getString(R.string.toast_validation_connexion)
                Toasty.success(this, toastConnexionOK, Toast.LENGTH_SHORT, true).show();
                Log.i("Info",toastConnexionOK)
            } else {
                val toastConnexionEchec = getString(R.string.toast_erreur_connexion)
                Toasty.error(this, toastConnexionEchec, Toast.LENGTH_SHORT, true).show();
                Log.i("Info",toastConnexionEchec)
            }
        }
    }

    private fun verificationConnexionInternet() {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting
        if (!isConnected) {
            val toastInternetEchec = getString(R.string.toast_erreur_connexionInternet)
            Toasty  .warning(this,toastInternetEchec,Toast.LENGTH_LONG)
                    .show()
        }
    }

}
